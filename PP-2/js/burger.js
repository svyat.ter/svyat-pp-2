const btnOpen = document.querySelector('#burgerShow');
const btnClose = document.querySelector('#burgerClose');
const shadow = document.querySelector('.shadow');
const menu = document.querySelector('#burgerMenu');

btnOpen.addEventListener('click', function () {
    menu.classList.add('burger__menu--show');
    shadow.classList.add('shadow--show');
})

btnClose.addEventListener('click', function () {
    menu.classList.remove('burger__menu--show');
    shadow.classList.remove('shadow--show');


})

shadow.addEventListener('click', function () {
    menu.classList.remove('burger-menu--show');
    shadow.classList.remove('shadow--show');


})